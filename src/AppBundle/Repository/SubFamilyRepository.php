<?php
/**
 * Created by PhpStorm.
 * User: heleneshaikh
 * Date: 23/04/2017
 * Time: 13:16
 */

namespace AppBundle\Repository;

use AppBundle\Entity\Genus;
use Doctrine\ORM\EntityRepository;

class SubFamilyRepository extends EntityRepository
{

    function createAlphabeticalQueryBuilder()
    {
        return $this->createQueryBuilder('s')->orderBy('s.name', 'ASC');
    }

}